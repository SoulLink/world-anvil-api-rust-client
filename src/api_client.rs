use reqwest::{Client, Response, StatusCode};
use reqwest::header::{HeaderMap, HeaderValue, USER_AGENT};
use crate::endpoints::{Get, List, Patch, Put};
use crate::responses::error::{ApiError, IdentityError};
use crate::enums::Granularity;
use crate::responses::error::PatchResponseUnprocessableEntity;
use crate::responses::{IdentityResponse, ListResponse};
use crate::schemas::Reference;

const HEADER_X_APPLICATION_KEY: &str = "x-application-key";
const HEADER_X_AUTH_TOKEN: &str = "x-auth-token";
const MIME_TYPE_JSON: &str = "application/json";
pub const BASE_URL: &str = "https://www.worldanvil.com/api/external/boromir";
const IDENTITY_ENDPOINT: &str = "identity";

#[derive(Debug, Clone)]
pub struct ApiClient {
    base_url: String,
    client: Client,
    headers: HeaderMap,
    headers_json: HeaderMap,
}

impl ApiClient {

    /// Creates a new ApiClient instance.
    ///
    /// # Arguments
    ///
    /// * `user_agent` - The user agent to use for the API requests.
    /// * `world_anvil_application_key` - The application key to use for the API requests.
    /// * `world_anvil_authentication_token` - The authentication token to use for the API requests.
    ///
    /// # Example
    ///
    /// ```rust
    /// use waapiclient::ApiClient;
    ///
    /// #[tokio::main]
    /// async fn main() {
    ///     let api_client = ApiClient::new(
    ///         "World Anvil Api Client Test (Rust)",
    ///         env!("WORLD_ANVIL_APPLICATION_KEY"),
    ///         env!("WORLD_ANVIL_AUTHENTICATION_TOKEN"),
    ///     );
    ///
    ///     let identity_response = match api_client.identity().await {
    ///        Ok(response) => response,
    ///         Err(err) => {
    ///             println!("{}", err);
    ///             return;
    ///         }
    ///     };
    ///     assert!(identity_response.success);
    /// }
    /// ```
    pub fn new(user_agent: &str, world_anvil_application_key: &str, world_anvil_authentication_token: &str) -> Self {
        let mut headers = HeaderMap::new();
        headers.insert(USER_AGENT, HeaderValue::from_str(user_agent).unwrap());
        headers.insert(HEADER_X_APPLICATION_KEY, HeaderValue::from_str(world_anvil_application_key).unwrap());
        headers.insert(HEADER_X_AUTH_TOKEN, HeaderValue::from_str(world_anvil_authentication_token).unwrap());

        let mut headers_json = headers.clone();
        headers_json.insert(reqwest::header::CONTENT_TYPE, HeaderValue::from_str(MIME_TYPE_JSON).unwrap());

        let client = Client::builder()
            .default_headers(headers.clone())
            .build()
            .unwrap();

        Self {
            base_url: BASE_URL.to_string(),
            client,
            headers,
            headers_json,
        }
    }

    async fn execute_request(&self, endpoint: &str, id: Option<&str>, granularity: Option<Granularity>, method: reqwest::Method, body: Option<serde_json::Value>) -> Result<Response, ApiError> {
        let mut url = format!("{}/{}", self.base_url, endpoint);
        if let Some(id) = id {
            url.push_str(&format!("?id={}", id));
        }
        if let Some(granularity) = granularity {
            url.push_str(&format!("&granularity={}", granularity.as_string()));
        }

        let request_builder = match method {
            reqwest::Method::GET => self.client
                .get(url)
                .headers(self.headers.clone()),
            reqwest::Method::PATCH => {
                if let Some(body) = body {
                    self.client.patch(url)
                        .body(body.to_string())
                        .headers(self.headers_json.clone())
                } else {
                    return Err(ApiError::InvalidRequest("PATCH request requires a JSON body".to_string()));
                }
            }
            reqwest::Method::DELETE => self.client
                .delete(url)
                .headers(self.headers.clone()),
            reqwest::Method::POST =>
                if let Some(body) = body {
                    self.client
                        .post(url)
                        .headers(self.headers_json.clone())
                        .body(body.to_string())
                } else {
                    return Err(ApiError::InvalidRequest("POST request requires a JSON body".to_string()));
                },
            reqwest::Method::PUT =>
                if let Some(body) = body {
                    self.client
                        .put(url)
                        .headers(self.headers_json.clone())
                        .body(body.to_string())
                } else {
                    return Err(ApiError::InvalidRequest("PUT request requires a JSON body".to_string()));
                },
            _ => return Err(ApiError::InvalidRequest("Unsupported HTTP method".to_string())),
        };

        let response = request_builder.send().await.map_err(|err| {
            ApiError::ConnectionError(err.to_string())
        })?;

        match response.status() {
            StatusCode::OK => Ok(response),
            StatusCode::NOT_FOUND => Err(ApiError::NotFound(id.unwrap_or("").to_string())),
            StatusCode::UNAUTHORIZED => Err(ApiError::Unauthorized),
            StatusCode::FORBIDDEN => Err(ApiError::Forbidden),
            StatusCode::UNPROCESSABLE_ENTITY => {
                let response_body = response.text().await.map_err(|err| {
                    ApiError::ConnectionError(err.to_string())
                })?;
                let error_response: PatchResponseUnprocessableEntity = serde_json::from_str(&response_body).map_err(|err| {
                    ApiError::ResponseParseError(err.to_string(), response_body)
                })?;
                Err(ApiError::UnprocessableEntity(error_response))
            }
            _ => {
                let error_msg = format!("API request failed with status code: {}", response.status());
                Err(ApiError::InternalServerError(error_msg))
            }
        }
    }

    async fn execute_get_request(&self, endpoint: &str, id: &str, granularity: Granularity) -> Result<Response, ApiError> {
        self.execute_request(endpoint, Some(id), Some(granularity), reqwest::Method::GET, None).await
    }

    async fn execute_patch_request(&self, endpoint: &str, id: &str, body: &serde_json::Value) -> Result<Response, ApiError> {
        self.execute_request(endpoint, Some(id), None, reqwest::Method::PATCH, Some(body.clone())).await
    }

    async fn execute_put_request(&self, endpoint: &str, body: &serde_json::Value) -> Result<Response, ApiError> {
        self.execute_request(endpoint, None, None, reqwest::Method::PUT, Some(body.clone())).await
    }

    async fn execute_delete_request(&self, endpoint: &str, id: &str) -> Result<(), ApiError> {
        self.execute_request(endpoint, Some(id), None, reqwest::Method::DELETE, None).await?;
        Ok(())
    }

    async fn execute_post_request(&self, endpoint: &str, id: &str, limit: i32, offset: i32) -> Result<Response, ApiError> {
        self.execute_request(endpoint, Some(id), None, reqwest::Method::POST, Some(
            serde_json::json!({
                "limit": limit,
                "offset": offset
            })
        )).await
    }

    async fn retrieve_all_entities(&self, endpoint: &str, id: &str) -> Result<Vec<Reference>, ApiError> {
        let mut entities = Vec::new();
        let mut offset = 0;
        let limit = 50;
        loop {
            let response = self.execute_post_request(endpoint, id, limit, offset).await?;
            let body = response.text().await.map_err(|err| {
                ApiError::ConnectionError(err.to_string())
            })?;
            let list_response: ListResponse = serde_json::from_str(&body).map_err(|err| {
                ApiError::ResponseParseError(err.to_string(), body)
            })?;
            if list_response.entities.len() < limit as usize {
                entities.extend(list_response.entities);
                break;
            }
            entities.extend(list_response.entities);
            offset += limit;
        }
        Ok(entities)
    }

    /// Retrieves the identity of the user associated with the authentication token.
    /// Use this function as the first step to retrieve the user's ID and to verify that the
    /// authentication token is valid.
    ///
    /// # Example
    ///
    /// ```rust
    /// use waapiclient::ApiClient;
    ///
    /// #[tokio::main]
    /// async fn main() {
    ///    let api_client = ApiClient::new(
    ///         "World Anvil Api Client Test (Rust)",
    ///         env!("WORLD_ANVIL_APPLICATION_KEY"),
    ///         env!("WORLD_ANVIL_AUTHENTICATION_TOKEN"),
    ///     );
    ///
    ///     let identity_response = match api_client.identity().await {
    ///         Ok(response) => response,
    ///         Err(err) => {
    ///             println!("{}", err);
    ///             return;
    ///         }
    ///     };
    ///     println!("{:?}", identity_response);
    /// }
    pub async fn identity(&self) -> Result<IdentityResponse, IdentityError> {
        let url = format!("{}/{}", self.base_url, IDENTITY_ENDPOINT);
        let response = self.client
            .get(url)
            .headers(self.headers.clone())
            .send()
            .await.map_err(|err| {
                IdentityError::ConnectionError(err.to_string())
            })?;
        match response.status() {
            StatusCode::OK => {
                let body = response.text().await.map_err(|err| {
                    IdentityError::ConnectionError(err.to_string())
                })?;
                let identity_response: IdentityResponse = serde_json::from_str(&body).map_err(|err| {
                    IdentityError::ResponseParseError(err.to_string(), body)
                })?;
                Ok(identity_response)
            }
            StatusCode::UNAUTHORIZED => Err(IdentityError::Unauthorized),
            _ => {
                let error_msg = format!("API request failed with status code: {}", response.status());
                Err(IdentityError::InternalServerError(error_msg))
            }
        }
    }

    /// Retrieves an entity of the given type. The entity type must implement the [`Get`] trait.
    /// The entity must be owned by the user associated with the authentication token directly or
    /// be part of a world the user has reading access to.
    ///
    /// # Arguments
    ///
    /// * `id` - The ID of the entity to retrieve.
    /// * `granularity` - The granularity of the entity to retrieve.
    ///
    /// # Example
    ///
    /// In this example a world is created and then retrieved.
    /// Replace the [`World`](crate::schemas::World) type with the type of the entity you want
    /// to retrieve.
    ///
    /// Other entities that work with this function are:
    ///
    /// * [`Article`](crate::schemas::Article) - Retrieve an article.
    /// * [`Image`](crate::schemas::Image) - Retrieve an image.
    /// * [`World`](crate::schemas::World) - Retrieve a world.
    ///
    /// ```rust
    /// use waapiclient::ApiClient;
    /// use waapiclient::schemas::World;
    /// use waapiclient::schemas::WorldCreate;
    /// use waapiclient::enums::Granularity;
    ///
    /// #[tokio::main]
    /// async fn main() {
    ///     let api_client = ApiClient::new(
    ///         "World Anvil Api Client Test (Rust)",
    ///         env!("WORLD_ANVIL_APPLICATION_KEY"),
    ///         env!("WORLD_ANVIL_AUTHENTICATION_TOKEN"),
    ///     );
    ///
    ///     let new_world = match api_client.put::<WorldCreate>(WorldCreate::new("Test Get World Endpoint")).await {
    ///         Ok(world) => world,
    ///         Err(err) => {
    ///             println!("{}", err);
    ///             return;
    ///         }
    ///     };
    ///
    ///     let world = match api_client.get::<World>(new_world.id.into(), Granularity::Two).await {
    ///         Ok(world) => world,
    ///         Err(err) => {
    ///             println!("{}", err);
    ///             return;
    ///         }
    ///     };
    ///     println!("{:?}", world);
    ///
    ///     let success = match api_client.delete::<World>(world.id.into()).await {
    ///         Ok(_) => true,
    ///         Err(err) => false,
    ///     };
    ///     println!("{}" , success);
    /// }
    pub async fn get<T: Get>(&self, id: String, granularity: Granularity) -> Result<T, ApiError> {
        let endpoint = T::get_endpoint();
        let response = self.execute_get_request(&endpoint, &id, granularity).await?;
        let body = response.text().await.map_err(|err| {
            ApiError::ConnectionError(err.to_string())
        })?;
        let entity: T = serde_json::from_str(&body).map_err(|err| {
            ApiError::ResponseParseError(err.to_string(), body.clone())
        })?;
        Ok(entity)
    }

    /// Updates an entity of the given type. The entity type must implement the [`Patch`] trait.
    /// The entity must be owned by the user associated with the authentication token directly or
    /// be part of a world the user has editing access to.
    ///
    /// # Arguments
    ///
    /// * `id` - The ID of the entity to update.
    /// * `body` - The body of the entity to update.
    ///
    /// # Example
    ///
    /// In this example a world is created and then updated and deleted.
    /// Replace the [`WorldUpdate`](crate::schemas::WorldUpdate) type with the type of the entity you want
    /// to update.
    ///
    /// Other entities that work with this function are:
    ///
    /// * [`ArticleUpdate`](crate::schemas::ArticleUpdate) - Update an article.
    /// * [`WorldUpdate`](crate::schemas::WorldUpdate) - Update a world.
    /// * [`ImageUpdate`](crate::schemas::ImageUpdate) - Update an image.
    ///
    /// ```rust
    /// use waapiclient::ApiClient;
    /// use waapiclient::schemas::World;
    /// use waapiclient::schemas::WorldCreate;
    /// use waapiclient::schemas::WorldUpdate;
    /// use waapiclient::enums::Granularity;
    ///
    /// #[tokio::main]
    /// async fn main() {
    ///     let api_client = ApiClient::new(
    ///         "World Anvil Api Client Test (Rust)",
    ///         env!("WORLD_ANVIL_APPLICATION_KEY"),
    ///         env!("WORLD_ANVIL_AUTHENTICATION_TOKEN"),
    ///     );
    ///
    ///
    ///     let new_world = match api_client.put::<WorldCreate>(WorldCreate::new("A Rust Test World")).await {
    ///         Ok(world) => world,
    ///         Err(err) => {
    ///             println!("{}", err);
    ///             return;
    ///         }
    ///     };
    ///
    ///     let world = match api_client.get::<World>(new_world.id.into(), Granularity::Two).await {
    ///         Ok(world) => world,
    ///         Err(err) => {
    ///             println!("{}", err);
    ///             return;
    ///         }
    ///     };
    ///
    ///     let updated_world = match api_client.patch::<WorldUpdate>(world.id.into(),
    ///             WorldUpdate::new()
    ///                 .title("A Rust Test World Updated"))
    ///             .await {
    ///         Ok(world) => world,
    ///         Err(err) => {
    ///             println!("{}", err);
    ///             return;
    ///         }
    ///     };
    ///     println!("{:?}", updated_world);
    ///
    ///     let success = match api_client.delete::<World>(updated_world.id.into()).await {
    ///         Ok(_) => true,
    ///         Err(err) => false,
    ///     };
    ///     println!("{}" , success);
    /// }
    /// ```
    pub async fn patch<T: Patch>(&self, id: String, body: T) -> Result<Reference, ApiError> {
        let endpoint = T::patch_endpoint();
        let response = self.execute_patch_request(&endpoint, &id, &serde_json::json!(body)).await?;
        let body = response.text().await.map_err(|err| {
            ApiError::ConnectionError(err.to_string())
        })?;
        let entity: Reference = serde_json::from_str(&body).map_err(|err| {
            ApiError::ResponseParseError(err.to_string(), body)
        })?;
        Ok(entity)
    }

    /// Creates a new entity of the given type. The entity type must implement the [`Put`] trait.
    ///
    /// # Arguments
    ///
    /// * `body` - The body of the entity to create.
    ///
    /// # Example
    ///
    /// In this example a new world is created. Replace the [`CreateWorld`](crate::schemas::WorldCreate) type with the type of the entity you want.
    ///
    /// Other entities that work with this function are:
    ///
    /// * [`CreateArticle`](crate::schemas::ArticleCreate) - Create a new article.
    /// * [`CreateWorld`](crate::schemas::WorldCreate) - Create a new world.
    ///
    /// ```rust
    /// use waapiclient::ApiClient;
    /// use waapiclient::schemas::WorldCreate;
    /// use waapiclient::schemas::World;
    ///
    /// #[tokio::main]
    /// async fn main() {
    ///     let api_client = ApiClient::new(
    ///         "World Anvil Api Client Test (Rust)",
    ///         env!("WORLD_ANVIL_APPLICATION_KEY"),
    ///         env!("WORLD_ANVIL_AUTHENTICATION_TOKEN"),
    ///     );
    ///
    ///     let identity_response = match api_client.identity().await {
    ///         Ok(response) => response,
    ///         Err(err) => {
    ///             println!("{}", err);
    ///             return;
    ///         }
    ///     };
    ///     let world = match api_client.put::<WorldCreate>(WorldCreate::new("Rust API Test World")).await {
    ///         Ok(world) => world,
    ///         Err(err) => {
    ///             println!("{}", err);
    ///             return;
    ///         }
    ///     };
    ///     println!("{:?}", world);
    ///
    ///     let success = match api_client.delete::<World>(world.id.into()).await {
    ///         Ok(_) => true,
    ///         Err(err) => false,
    ///     };
    ///     println!("{}" , success);
    /// }

    pub async fn put<T: Put>(&self, body: T) -> Result<Reference, ApiError> {
        let endpoint = T::put_endpoint();
        let response = self.execute_put_request(&endpoint, &serde_json::json!(body)).await?;
        let body = response.text().await.map_err(|err| {
            ApiError::ConnectionError(err.to_string())
        })?;
        let entity: Reference = serde_json::from_str(&body).map_err(|err| {
            ApiError::ResponseParseError(err.to_string(), body)
        })?;
        Ok(entity)
    }

    /// Deletes the entity with the given ID. The entity type must implement the [`Get`] trait.
    /// The entity must be owned by the user associated with the authentication token.
    ///
    /// # Arguments
    ///
    /// * `id` - The ID of the entity to delete.
    ///
    /// # Example
    ///
    /// In this example an world is created and then deleted.
    /// Replace the [`World`](crate::schemas::World) type with the type of the entity you want
    /// to delete.
    ///
    /// Other entities that work with this function are:
    /// * [`Image`](crate::schemas::Image) - Delete an image.
    /// * [`Article`](crate::schemas::Article) - Delete an article.
    /// * [`World`](crate::schemas::World) - Delete a world.
    ///
    /// ```rust
    /// use waapiclient::ApiClient;
    /// use waapiclient::schemas::World;
    /// use waapiclient::schemas::WorldCreate;
    ///
    /// #[tokio::main]
    /// async fn main() {
    ///     let api_client = ApiClient::new(
    ///         "World Anvil Api Client Test (Rust)",
    ///         env!("WORLD_ANVIL_APPLICATION_KEY"),
    ///         env!("WORLD_ANVIL_AUTHENTICATION_TOKEN"),
    ///     );
    ///
    ///     let identity_response = match api_client.identity().await {
    ///         Ok(response) => response,
    ///         Err(err) => {
    ///             println!("{}", err);
    ///             return;
    ///         }
    ///      };
    ///
    ///     let world = match api_client.put::<WorldCreate>(WorldCreate::new("Rust API Test World")).await {
    ///         Ok(world) => world,
    ///         Err(err) => {
    ///             println!("{}", err);
    ///             return;
    ///         }
    ///     };
    ///
    ///     let success = match api_client.delete::<World>(world.id.into()).await {
    ///         Ok(_) => true,
    ///         Err(err) => false,
    ///     };
    ///     println!("{}" , success);
    ///}
    /// ```
    pub async fn delete<T: Get>(&self, id: String) -> Result<(), ApiError> {
        let endpoint = T::get_endpoint();
        self.execute_delete_request(&endpoint, &id).await?;
        Ok(())
    }


    /// Retrieves all entities of the given type that are listed under another resource.
    /// Check the list in the examples section which entities are supported
    /// and the id of what type of entity you need to pass.
    ///
    /// # Arguments
    ///
    /// * `id` - The id of the parent resource.
    ///
    /// # Example
    ///
    /// In this example a list of worlds is retrieved for the user associated with the
    /// authentication token. Replace the [`World`](crate::schemas::World) type with the type of the entities you want
    /// to get.
    ///
    /// Other entities that work with this function are:
    /// * [`Article`](crate::schemas::Article) - Add a [`World`](crate::schemas::World) id to retrieve articles for a specific world.
    /// * [`Image`](crate::schemas::Image) - Add a [`World`](crate::schemas::World) id to retrieve images for a specific world.
    ///
    /// ```rust
    /// use waapiclient::ApiClient;
    /// use waapiclient::schemas::world::World;
    ///
    /// #[tokio::main]
    /// async fn main() {
    ///     let api_client = ApiClient::new(
    ///         "World Anvil Api Client Test (Rust)",
    ///         env!("WORLD_ANVIL_APPLICATION_KEY"),
    ///         env!("WORLD_ANVIL_AUTHENTICATION_TOKEN"),
    ///     );
    ///
    ///     let identity_response = match api_client.identity().await {
    ///         Ok(response) => response,
    ///         Err(err) => {
    ///             println!("{}", err);
    ///             return;
    ///         }
    ///     };
    ///     let entities = match api_client.list::<World>(&identity_response.id).await {
    ///         Ok(entities) => entities,
    ///         Err(err) => {
    ///             println!("{}", err);
    ///             return;
    ///         }
    ///     };
    ///     println!("{:?}" , entities);
    /// }
    pub async fn list<T: List>(&self, id: &str) -> Result<Vec<Reference>, ApiError> {
        let endpoint = T::list_endpoint();
        let entities = self.retrieve_all_entities(&endpoint, id).await?;
        Ok(entities)
    }
}
