use serde::{Deserialize, Serialize};
use crate::endpoints::{Get, Patch};
use crate::schemas::date_time_info::DateTimeInfo;
use crate::enums::EntityClass;
use crate::schemas::Reference;

#[derive(Debug, Serialize, Deserialize, Eq, PartialEq, Clone)]
#[serde(rename_all = "camelCase")]
pub struct User {
    pub id: String,
    pub title: String,
    pub entity_class: EntityClass,
    pub icon: String,
    pub url: String,
    token: Option<String>,
    pub username: Option<String>,
    pub firstname: Option<String>,
    pub lastname: Option<String>,
    pub last_login: Option<DateTimeInfo>,
    pub bio: Option<String>,
    pub locale: Option<String>,
    pub signature: Option<String>,
    pub custom_profile_content: Option<String>,
    pub register_date: Option<DateTimeInfo>,
    pub membership_date: Option<DateTimeInfo>,
    pub membership: Option<bool>,
    pub membership_type: Option<String>,
    pub websiteurl: Option<String>,
    pub fav_movies: Option<String>,
    pub fav_series: Option<String>,
    pub fav_books: Option<String>,
    pub fav_writers: Option<String>,
    pub fav_games: Option<String>,
    pub interests: Option<String>,
    pub nanowrimo: Option<String>,
    pub twitter: Option<String>,
    pub facebook: Option<String>,
    pub reddit: Option<String>,
    pub tumblr: Option<String>,
    pub pinterest: Option<String>,
    pub deviantart: Option<String>,
    pub youtube: Option<String>,
    pub vimeo: Option<String>,
    pub google: Option<String>,
    pub steam: Option<String>,
    pub twitch: Option<String>,
    pub discord: Option<String>,
    pub instagram: Option<String>,
    pub kofi: Option<String>,
    pub patreon: Option<String>,
    pub views: Option<i32>,
    pub open_text: Option<String>,
    pub profile_meta: Option<String>,
    pub is_venerable: Option<bool>,
    pub is_premier: Option<bool>,
    pub is_lifetime: Option<bool>,
    pub anvil_coins_current: Option<i32>,
    pub email: Option<String>,
    pub timezone: Option<String>,
    pub location: Option<String>,
    pub dob: Option<String>,
    pub worlds_limit: Option<i32>,
    pub campaigns_limit: Option<i32>,
    pub upload_size_limit: Option<i32>,
    pub storage_space_limit: Option<String>,
    pub coauthors: Option<i32>,
    pub subscriber_slots: Option<i32>,
    pub allow_adult_content: Option<bool>,
    pub feature_worldbuilding: Option<i32>,
    #[serde(rename = "featureRPG")]
    pub feature_rpg: Option<i32>,
    pub feature_community: Option<i32>,
    pub feature_heroes: Option<i32>,
    pub feature_writing: Option<i32>,
    pub feature_prompts: Option<i32>,
    pub feature_autosave: Option<i32>,
    pub feature_expanded_article: Option<i32>,
    pub interface_version: Option<i32>,
    pub interface_vignette_rows: Option<String>,
    pub interface_form_background: Option<String>,
    pub interface_form_color: Option<String>,
    pub interface_form_font_size: Option<String>,
    pub interface_activate_advanced_select: Option<bool>,
    pub interface_activate_accessibility: Option<bool>,
    pub manuscript_settings: Option<ManuscriptSettings>,
    pub interface_theme: Option<String>,
    pub interface_editor: Option<String>,
    pub interface_editor_mode: Option<String>,
    pub interface_editor_theme: Option<String>,
    pub interface_show_save_indicator: Option<bool>,
    pub discord_token: Option<String>,
    pub discord_refresh_token: Option<String>,
    pub discord_token_expiry: Option<String>,
    pub discord_user_id: Option<String>,
    pub is_newsletter: Option<bool>,
    pub membership_prototype: Option<MembershipPrototype>,
    #[serde(rename = "chapterhouse")]
    pub chapter_house: Option<String>,
    pub active_world: Option<Reference>,
    pub active_campaign: Option<Reference>,
    pub active_character: Option<Reference>,
    pub active_session: Option<Reference>,
    pub cover: Option<Reference>,
    pub avatar: Option<Reference>
}

impl Get for User {
    fn get_endpoint() -> String {
        "user".to_string()
    }
}

#[derive(Debug, Serialize, Deserialize, Eq, PartialEq, Clone)]
#[serde(rename_all = "camelCase")]
pub struct UserChange {
    pub title: Option<String>,
    pub username: Option<String>,
    pub firstname: Option<String>,
    pub lastname: Option<String>,
    pub bio: Option<String>,
    pub locale: Option<String>,
    pub signature: Option<String>,
    pub custom_profile_content: Option<String>,
    pub websiteurl: Option<String>,
    pub fav_movies: Option<String>,
    pub fav_series: Option<String>,
    pub fav_books: Option<String>,
    pub fav_writers: Option<String>,
    pub fav_games: Option<String>,
    pub interests: Option<String>,
    pub nanowrimo: Option<String>,
    pub twitter: Option<String>,
    pub facebook: Option<String>,
    pub reddit: Option<String>,
    pub tumblr: Option<String>,
    pub pinterest: Option<String>,
    pub deviantart: Option<String>,
    pub youtube: Option<String>,
    pub vimeo: Option<String>,
    pub google: Option<String>,
    pub steam: Option<String>,
    pub twitch: Option<String>,
    pub discord: Option<String>,
    pub instagram: Option<String>,
    pub kofi: Option<String>,
    pub patreon: Option<String>,
    pub email: Option<String>,
    pub timezone: Option<String>,
    pub location: Option<String>,
    pub dob: Option<String>,
    pub allow_adult_content: Option<bool>,
    pub feature_worldbuilding: Option<i32>,
    #[serde(rename = "featureRPG")]
    pub feature_rpg: Option<i32>,
    pub feature_community: Option<i32>,
    pub feature_heroes: Option<i32>,
    pub feature_writing: Option<i32>,
    pub feature_prompts: Option<i32>,
    pub feature_autosave: Option<i32>,
    pub feature_expanded_article: Option<i32>,
    pub interface_version: Option<i32>,
    pub interface_vignette_rows: Option<String>,
    pub interface_form_background: Option<String>,
    pub interface_form_color: Option<String>,
    pub interface_form_font_size: Option<String>,
    pub interface_activate_advanced_select: Option<bool>,
    pub interface_activate_accessibility: Option<bool>,
    pub manuscript_settings: Option<ManuscriptSettings>,
    pub interface_theme: Option<String>,
    pub interface_editor: Option<String>,
    pub interface_editor_mode: Option<String>,
    pub interface_editor_theme: Option<String>,
    pub interface_show_save_indicator: Option<bool>,
    pub is_newsletter: Option<bool>,
    pub active_world: Option<Reference>,
    pub active_campaign: Option<Reference>,
    pub active_character: Option<Reference>,
    pub active_session: Option<Reference>,
    pub cover: Option<Reference>,
    pub avatar: Option<Reference>
}

impl Patch for User {
    fn patch_endpoint() -> String {
        "user".to_string()
    }
}

#[derive(Debug, Serialize, Deserialize, Eq, PartialEq, Clone)]
#[serde(rename_all = "camelCase")]
pub struct ManuscriptSettings {
    pub font_size: String,
    pub line_height: String,
    pub paragraphy_padding: String,
    pub css_rules: String,
    pub font_typeface: String,
    pub paragraph_indent: String,
    pub background_color: String,
    pub font_color: String,
    pub paragraph_padding: String
}

#[derive(Debug, Serialize, Deserialize, Eq, PartialEq, Clone)]
pub struct MembershipPrototype {
    pub id: String,
    pub title: String
}