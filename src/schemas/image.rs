use serde::{Deserialize, Serialize};
use serde_with::skip_serializing_none;
use crate::endpoints::{Delete, Get, List, Patch};
use crate::schemas::date_time_info::DateTimeInfo;
use crate::enums::{EntityClass, State};
use crate::schemas::{Reference, UuidReference};


/// The full image object. This is the object that is returned when you get an image.
#[derive(Debug, Serialize, Deserialize, Eq, PartialEq, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Image {
    pub id: i32,
    pub title: String,
    pub slug: Option<String>,
    pub state: State,
    pub is_wip: Option<bool>,
    pub is_draft: Option<bool>,
    pub entity_class: EntityClass,
    pub icon: String,
    pub url: String,
    #[serde(rename = "subscribergroups")]
    pub subscriber_groups: Vec<Reference>,
    pub folder_id: Option<String>,
    pub tags: Option<String>,
    pub update_date: DateTimeInfo,
    pub filename: Option<String>,
    pub path: Option<String>,
    pub size: Option<i32>,
    pub width: Option<i32>,
    pub height: Option<i32>,
    pub extension: Option<String>,
    pub description: Option<String>,
    pub alt: Option<String>,
    pub credit_artist_name: Option<String>,
    pub credit_artist_website: Option<String>,
    pub credit_art_title: Option<String>,
    pub credit_art_url:Option< String>,
    pub creation_date: Option<DateTimeInfo>,
    pub views: Option<i32>,
    pub likes: Option<i32>,
    pub is_featured: Option<bool>,
    pub link_url: Option<String>,
    pub page_url: Option<String>,
    pub article: Option<Reference>,
    pub owner: Option<Reference>,
    pub world: Option<Reference>,
    pub character: Option<Reference>,
    pub galleries: Option<Vec<Reference>>
}

impl Get for Image {
    fn get_endpoint() -> String {
        "image".to_string()
    }
}

impl Delete for Image {
    fn delete_endpoint() -> String {
        "image".to_string()
    }
}

impl List for Image {
    fn list_endpoint() -> String {
        "world/images".to_string()
    }
}

/// The image object used for updating an image.
#[skip_serializing_none]
#[derive(Debug, Serialize, Deserialize, Eq, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct ImagePatch {
    pub title: Option<String>,
    pub state: Option<State>,
    #[serde(rename = "subscribergroups")]
    pub subscriber_groups: Option<Vec<UuidReference>>,
    pub tags: Option<Option<String>>,
    pub description: Option<Option<String>>,
    pub alt: Option<Option<String>>,
    pub credit_artist_name: Option<Option<String>>,
    pub credit_artist_website: Option<Option<String>>,
    pub credit_art_title: Option<Option<String>>,
    pub credit_art_url: Option<Option< String>>,
    pub is_featured: Option<bool>,
    pub link_url: Option<Option<String>>,
    pub article: Option<Option<UuidReference>>,
    pub world: Option<UuidReference>,
    pub character: Option<Option<UuidReference>>,
}


impl ImagePatch {
    pub fn new() -> Self {
        ImagePatch {
            title: None,
            state: None,
            subscriber_groups: None,
            tags: None,
            description: None,
            alt: None,
            credit_artist_name: None,
            credit_artist_website: None,
            credit_art_title: None,
            credit_art_url: None,
            is_featured: None,
            link_url: None,
            article: None,
            world: None,
            character: None
        }
    }

    pub fn title(mut self, title: &str) -> Self {
        self.title = Some(title.to_string());
        self
    }

    pub fn state(mut self, state: State) -> Self {
        self.state = Some(state);
        self
    }

    /// The subscriber groups that can view this image. To remove all subscriber groups, pass an
    /// empty array. To add a subscriber group, pass an array with the UUID of the subscriber group
    /// and make sure all other subscriber groups are included. To remove a subscriber group, pass
    /// an array with all subscriber groups except the one you want to remove.
    pub fn subscriber_groups(mut self, subscriber_groups: Vec<UuidReference>) -> Self {
        self.subscriber_groups = Some(subscriber_groups);
        self
    }

    /// The tags for this image. To remove all tags, pass an empty string. To add a tag, pass a
    /// comma-separated list of tags and make sure all other tags are included. To remove a tag,
    /// pass a comma-separated list of tags except the one you want to remove.
    pub fn tags(mut self, tags: Option<&str>) -> Self {
        self.tags = Some(tags.map(|s| s.to_string()));
        self
    }

    /// The description of this image. To remove the description, pass None.
    pub fn description(mut self, description: Option<&str>) -> Self {
        self.description = Some(description.map(|s| s.to_string()));
        self
    }

    /// The alt text for this image. To remove the alt text, pass None.
    pub fn alt(mut self, alt: Option<&str>) -> Self {
        self.alt = Some(alt.map(|s| s.to_string()));
        self
    }

    /// The name of the artist who created this image. To remove the artist name, pass None.
    pub fn credit_artist_name(mut self, credit_artist_name: Option<&str>) -> Self {
        self.credit_artist_name = Some(credit_artist_name.map(|s| s.to_string()));
        self
    }

    /// The website of the artist who created this image. To remove the artist website, pass None.
    pub fn credit_artist_website(mut self, credit_artist_website: Option<&str>) -> Self {
        self.credit_artist_website = Some(credit_artist_website.map(|s| s.to_string()));
        self
    }

    /// The title of the art piece this image is based on. To remove the art title, pass None.
    pub fn credit_art_title(mut self, credit_art_title: Option<&str>) -> Self {
        self.credit_art_title = Some(credit_art_title.map(|s| s.to_string()));
        self
    }

    /// The URL of the art piece this image is based on. To remove the art URL, pass None.
    pub fn credit_art_url(mut self, credit_art_url: Option<&str>) -> Self {
        self.credit_art_url = Some(credit_art_url.map(|s| s.to_string()));
        self
    }

    /// Whether this image is featured.
    pub fn is_featured(mut self, is_featured: bool) -> Self {
        self.is_featured = Some(is_featured);
        self
    }

    /// The URL this image links to. To remove the link URL, pass None.
    pub fn link_url(mut self, link_url: Option<&str>) -> Self {
        self.link_url = Some(link_url.map(|s| s.to_string()));
        self
    }

    /// The article this image is attached to. To remove the article, pass None.
    pub fn article(mut self, article: Option<&str>) -> Self {
        self.article = Some(article.map(|s| UuidReference { id: s.to_string() }));
        self
    }

    /// Change the world this image is attached to.
    pub fn world(mut self, world: &str) -> Self {
        self.world = Some(UuidReference { id: world.to_string() });
        self
    }

    /// The character this image is attached to. To remove the character, pass None.
    pub fn character(mut self, character: Option<&str>) -> Self {
        self.character = Some(character.map(|s| UuidReference { id: s.to_string() }));
        self
    }

}

impl Patch for ImagePatch {
    fn patch_endpoint() -> String {
        "image".to_string()
    }
}