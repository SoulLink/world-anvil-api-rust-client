use serde::{Deserialize, Serialize};
use serde_with::skip_serializing_none;
use crate::endpoints::{Get, List, Patch, Put};
use crate::enums::{EntityClass, State};
use crate::schemas::UuidReference;

#[derive(Debug, Serialize, Deserialize, Eq, PartialEq, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Article {
    pub id: String,
    pub title: String,
    pub state: State,
    pub slug: String,
    pub entity_class: EntityClass,
    pub icon: String,
    pub url: String,
    pub template_type: String,
    pub content: Option<String>,
}

impl Get for Article {
    fn get_endpoint() -> String {
        "article".to_string()
    }
}

impl List for Article {
    fn list_endpoint() -> String {
        String::from("world/articles")
    }
}

#[skip_serializing_none]
#[derive(Debug, Serialize, Deserialize, Eq, PartialEq, Clone)]
#[serde(rename_all = "camelCase")]
pub struct ArticleCreate {
    pub title: String,
    pub state: Option<State>,
    pub icon: Option<String>,
    pub template_type: String,
    pub world: UuidReference,
}

impl ArticleCreate {
    pub fn new(title: &str, template_type: &str, world: &str) -> Self {
        ArticleCreate {
            title: title.to_string(),
            state: None,
            icon: None,
            template_type: template_type.to_string(),
            world: world.into()
        }
    }

    pub fn with_state(mut self, state: State) -> Self {
        self.state = Some(state);
        self
    }

    pub fn with_icon(mut self, icon: &str) -> Self {
        self.icon = Some(icon.to_string());
        self
    }
}

impl Put for ArticleCreate {
    fn put_endpoint() -> String {
        String::from("article")
    }
}


#[skip_serializing_none]
#[derive(Debug, Serialize, Deserialize, Eq, PartialEq, Clone)]
#[serde(rename_all = "camelCase")]
pub struct ArticleUpdate {
    pub title: Option<String>,
    pub state: Option<State>,
    pub icon: Option<Option<String>>,
    pub content: Option<Option<String>>,
}


impl ArticleUpdate {
    pub fn new() -> Self {
        ArticleUpdate {
            title: None,
            state: None,
            icon: None,
            content: None,
        }
    }

    pub fn title(mut self, title: &str) -> Self {
        self.title = Some(title.to_string());
        self
    }

    pub fn state(mut self, state: State) -> Self {
        self.state = Some(state);
        self
    }

    pub fn icon(mut self, icon: &str) -> Self {
        self.icon = Some(Some(icon.to_string()));
        self
    }

    pub fn content(mut self, content: &str) -> Self {
        self.content = Some(Some(content.to_string()));
        self
    }
}


impl Patch for ArticleUpdate {
    fn patch_endpoint() -> String {
        String::from("article")
    }
}