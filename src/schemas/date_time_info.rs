use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Eq, PartialEq, Clone)]
pub struct DateTimeInfo {
    pub date: String,
    pub timezone: String,
    pub timezone_type: i32,
}


impl DateTimeInfo {

    /// Create a new DateTimeInfo object with the given date.
    /// Uses UTC as the timezone and 3 as the timezone type. This is the default for the API.
    pub fn new(date: String) -> Self {
        DateTimeInfo {
            date,
            timezone: "UTC".to_string(),
            timezone_type: 3
        }
    }
}