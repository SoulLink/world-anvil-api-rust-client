use serde::{Deserialize, Serialize};
use serde_with::skip_serializing_none;
use crate::endpoints::{Get, List, Patch, Put};
use crate::enums::{EntityClass, State};

#[derive(Debug, Serialize, Deserialize, Eq, PartialEq, Clone)]
#[serde(rename_all = "camelCase")]
pub struct World {
    pub id: String,
    pub title: String,
    pub state: State,
    pub slug: String,
    pub entity_class: EntityClass,
    pub icon: String,
    pub url: String
}

impl Get for World {
    fn get_endpoint() -> String {
        "world".to_string()
    }
}

impl List for World {
    fn list_endpoint() -> String {
        String::from("user/worlds")
    }
}


#[derive(Debug, Serialize, Deserialize, Eq, PartialEq, Clone)]
#[serde(rename_all = "camelCase")]
pub struct WorldCreate {
    pub title: String
}

impl WorldCreate {
    pub fn new(title: &str) -> Self {
        WorldCreate {
            title: title.to_string()
        }
    }
}

impl Put for WorldCreate {
    fn put_endpoint() -> String {
        String::from("world")
    }
}

#[skip_serializing_none]
#[derive(Debug, Serialize, Deserialize, Eq, PartialEq, Clone)]
#[serde(rename_all = "camelCase")]
pub struct WorldUpdate {
    pub title: Option<String>
}

impl WorldUpdate {
    pub fn new() -> Self {
        WorldUpdate {
            title: None
        }
    }

    pub fn title(mut self, title: &str) -> Self {
        self.title = Some(title.to_string());
        self
    }
}

impl Patch for WorldUpdate {
    fn patch_endpoint() -> String {
        String::from("world")
    }
}
