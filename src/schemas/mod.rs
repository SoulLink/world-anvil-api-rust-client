use serde::{Deserialize, Serialize};
use crate::enums::{EntityClass, State};

pub mod user;
pub mod date_time_info;
pub mod image;
pub mod subscriber_group;
pub mod world;
pub mod article;

pub use user::User;
pub use date_time_info::DateTimeInfo;
pub use image::Image;
pub use world::World;
pub use world::WorldCreate;
pub use world::WorldUpdate;
pub use article::Article;


/// An object of UUID reference to update an entity.
#[derive(Debug, Serialize, Deserialize, Eq, PartialEq, Clone)]
pub struct UuidReference {
    pub id: String
}

impl From<&str> for UuidReference {
    fn from(id: &str) -> Self {
        UuidReference {
            id: id.to_string()
        }
    }
}

/// An object of Id reference to update an entity.
#[derive(Debug, Serialize, Deserialize, Eq, PartialEq, Clone)]
pub struct IdReference {
    pub id: i32
}



/// This is the basic return object for all entities. It contains the basic information about the entity,
/// including the entity class, which is used to determine the type of entity.
#[derive(Debug, Serialize, Deserialize, Eq, PartialEq, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Reference {
    pub id: ReferenceId,
    pub title: String,
    pub slug: Option<String>,
    pub state: Option<State>,
    pub is_wip: Option<bool>,
    pub is_draft: Option<bool>,
    pub entity_class: EntityClass,
    pub icon: Option<String>,
    pub url: Option<String>,
    #[serde(rename = "subscribergroups")]
    pub subscriber_groups: Vec<Reference>,
    pub tags: Option<String>,
    pub update_date: Option<DateTimeInfo>,
}

#[derive(Debug, Serialize, Deserialize, Eq, PartialEq, Clone)]
#[serde(untagged)]
pub enum ReferenceId {
    StringId(String),
    IntId(i32),
}

impl Into<String> for ReferenceId {
    fn into(self) -> String {
        match self {
            ReferenceId::StringId(id) => id,
            ReferenceId::IntId(id) => id.to_string()
        }
    }
}

impl From<String> for ReferenceId {
    fn from(id: String) -> Self {
        ReferenceId::StringId(id)
    }
}

impl From<i32> for ReferenceId {
    fn from(id: i32) -> Self {
        ReferenceId::IntId(id)
    }
}