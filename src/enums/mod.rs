use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Eq, PartialEq, Clone)]
#[allow(non_camel_case_types)]
pub enum State {
    public,
    private,
    visible,
    hidden,
    /// this is the case when a resource is deleted, but still referenced somewhere
    archived,
}

#[derive(Debug, Serialize, Deserialize, Eq, PartialEq, Clone)]
#[allow(non_camel_case_types)]
pub enum StateVisibility {
    visible,
    hidden,
}

#[derive(Debug, Serialize, Deserialize, Eq, PartialEq, Clone)]
pub enum EntityClass {
    Image,
    Gallery,
    User,
    World,
    Character,
    Campaign,
    SubscriberGroup,
    Session,
    // Articles
    Article,
    Report,
    Ethnicity,
    Landmark,
    Location,
    Ritual,
    Myth,
    Technology,
    Spell,
    Law,
    Prose,
    MilitaryConflict,
    Language,
    Document,
    Person,
    Organization,
    Plot,
    Species,
    Vehicle,
    Profession,
    Item,
    Formation,
    Rank,
    Condition,
    Material,
    Settlement,
}

#[derive(Debug, Serialize, Deserialize, Eq, PartialEq, Copy, Clone)]
pub enum Granularity {
    Reference,
    Zero,
    One,
    Two,
    Three,
}

impl Granularity {
    pub fn as_string(&self) -> String {
        match self {
            Granularity::Reference => "-1".to_string(),
            Granularity::Zero => "0".to_string(),
            Granularity::One => "1".to_string(),
            Granularity::Two => "2".to_string(),
            Granularity::Three => "3".to_string(),
        }
    }
}
