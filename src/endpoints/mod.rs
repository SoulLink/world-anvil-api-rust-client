use serde::{Deserialize, Serialize};
use serde_json::Value;

pub trait Get
where Self: for<'a> Deserialize<'a>
{
    fn get_endpoint() -> String;
}

pub trait Put
where Self: Serialize,
{
    fn put_endpoint() -> String;

    fn to_json(&self) -> Value {
        serde_json::to_value(self).expect("Failed to serialize to JSON. Something went wrong.")
    }
}

pub trait Patch
    where Self: Serialize
{
    fn patch_endpoint() -> String;

    fn to_json(&self) -> Value {
        serde_json::to_value(self).expect("Failed to serialize to JSON. Something went wrong.")
    }
}

pub trait Delete {
    fn delete_endpoint() -> String;
}

pub trait List {
    fn list_endpoint() -> String;
}