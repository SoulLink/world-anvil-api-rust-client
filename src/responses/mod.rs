use serde::{Deserialize, Serialize};
use crate::schemas::Reference;

pub mod error;

/// This is the response of the identity endpoint.
/// It contains the user's id, username, userhash and a success flag.
#[derive(Debug, Serialize, Deserialize)]
pub struct IdentityResponse {
    pub id: String,
    pub success: bool,
    pub username: String,
    pub userhash: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ListResponse {
    pub entities: Vec<Reference>,
    pub success: bool,
}