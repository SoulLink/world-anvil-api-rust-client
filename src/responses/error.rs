use std::error::Error;
use std::fmt::{Display, Formatter};
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub enum ApiError {
    NotFound(String),
    Unauthorized,
    Forbidden,
    UnprocessableEntity(PatchResponseUnprocessableEntity),
    ResponseParseError(String, String),
    ConnectionError(String),
    InternalServerError(String),
    InvalidRequest(String),
}

impl Error for ApiError {}

impl Display for ApiError {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        match self {
            ApiError::NotFound(id) => write!(f, "Not Found (404). The requested resource '{}' could not be found.", id),
            ApiError::Unauthorized => write!(f, "Unauthorized (401). No valid authentication provided."),
            ApiError::Forbidden => write!(f, "Forbidden (403). The provided authentication is not allowed to access this resource."),
            ApiError::UnprocessableEntity(msg) => write!(f, "Unprocessable Entity (422): {:?}", msg),
            ApiError::ResponseParseError(msg, content) => write!(f, "Response Parse Error: {}\n\n{}", msg, content),
            ApiError::ConnectionError(msg) => write!(f, "Connection Error: {}", msg),
            ApiError::InternalServerError(msg) => write!(f, "Internal Server Error: {}", msg),
            ApiError::InvalidRequest(msg) => write!(f, "Invalid Request: {}", msg)
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub enum IdentityError {
    Unauthorized,
    ConnectionError(String),
    ResponseParseError(String, String),
    InternalServerError(String),
}

impl Error for IdentityError {}

impl Display for IdentityError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            IdentityError::Unauthorized => write!(f, "Unauthorized (401). No valid authentication provided."),
            IdentityError::ConnectionError(msg) => write!(f, "Connection Error: {}", msg),
            IdentityError::InternalServerError(msg) => write!(f, "Internal Server Error: {}", msg),
            IdentityError::ResponseParseError(msg, content) => write!(f, "Response Parse Error: {}\n\n{}", msg, content),
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct PatchResponseUnprocessableEntity {
    pub success: bool,
    pub error: String,
    pub trace: String
}