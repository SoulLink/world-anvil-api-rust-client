# World Anvil API Rust Client
This client is a thin wrapper around the World Anvil Public API. It is intended to be used by Rust applications that 
need to interact with the World Anvil API. Note that this client is not officially supported by World Anvil.
## Features
- [x] Read Identity
- [x] User (Read, Update, List Worlds)
- [x] World (Read, List Articles)
- [x] Image Metadata (Read, Update, Delete)