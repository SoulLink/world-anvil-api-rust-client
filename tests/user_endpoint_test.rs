#[cfg(test)]
mod tests {
    use std::env;
    use tokio::test;
    use waapiclient::ApiClient;
    use waapiclient::enums::Granularity;
    use waapiclient::schemas::user::User;
    use waapiclient::schemas::world::World;

    async fn create_user_endpoint_and_id() -> (ApiClient, String) {
        let application_key = env::var("WORLD_ANVIL_APPLICATION_KEY").unwrap_or_else(|_| "test".to_string());
        let authentication_token = env::var("WORLD_ANVIL_AUTHENTICATION_TOKEN").unwrap_or_else(|_| "test".to_string());

        let api_client = ApiClient::new(
            "World Anvil Api Client Test/0.1.0 (Rust)",
            &application_key,
            &authentication_token,
        );
        let id = api_client.identity().await.unwrap().id;
        (api_client, id)
    }

    #[test]
    async fn test_user_endpoint_get_gr_neg_one_success() {
        let (api_client, id) = create_user_endpoint_and_id().await;
        match api_client.get::<User>(id, Granularity::Reference).await {
            Ok(user) => {
                assert_eq!(user.id, "42eb1d6a-021b-49b4-bbbb-f7ddf6b135a4");
                assert_eq!(user.title, "SoulLink");
                assert_eq!(user.url, "http://www.worldanvil.com/author/SoulLink");
            }
            Err(err) => {
                println!("{}", err);
                assert!(false, "Expected success result");
            }
        }
    }

    #[test]
    async fn test_user_endpoint_get_user_gr_0_success() {
        let (api_client, id) = create_user_endpoint_and_id().await;
        match api_client.get::<User>(id, Granularity::Zero).await {
            Ok(user) => {
                assert_eq!(user.id, "42eb1d6a-021b-49b4-bbbb-f7ddf6b135a4");
                assert_eq!(user.title, "SoulLink");
                assert_eq!(user.url, "http://www.worldanvil.com/author/SoulLink");
                assert_eq!(user.username, Some("SoulLink".to_string()));
            }
            Err(err) => {
                println!("{}", err);
                assert!(false, "Expected success result");
            }
        }
    }


    #[test]
    async fn test_user_endpoint_worlds() {
        let (api_client, id) = create_user_endpoint_and_id().await;

        match api_client.list::<World>(&id).await {
            Ok(worlds) => {
                println!("{:?}", worlds);
                assert_eq!(worlds.len(), 2);
            }
            Err(err) => {
                println!("{}", err);
                assert!(false, "Expected success result");
            }
        }
    }
}