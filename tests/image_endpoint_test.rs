#[cfg(test)]
mod tests {
    use std::env;
    use tokio::test;
    use waapiclient::ApiClient;
    use waapiclient::enums::{EntityClass, Granularity, State};
    use waapiclient::enums::EntityClass::SubscriberGroup;
    use waapiclient::schemas::image::{Image, ImagePatch};
    use waapiclient::schemas::Reference;
    use waapiclient::schemas::ReferenceId::StringId;

    async fn create_user_endpoint_and_id() -> ApiClient {
        let application_key = env::var("WORLD_ANVIL_APPLICATION_KEY").unwrap_or_else(|_| "test".to_string());
        let authentication_token = env::var("WORLD_ANVIL_AUTHENTICATION_TOKEN").unwrap_or_else(|_| "test".to_string());
        ApiClient::new(
            "World Anvil Api Client Test/0.1.0 (Rust)",
            &application_key,
            &authentication_token,
        )
    }

    #[test]
    async fn test_image_get_ref_success() {
        let api_client = create_user_endpoint_and_id().await;
        let result = api_client.get::<Image>("4864340".to_string(), Granularity::Reference).await;
        match result {
            Ok(image) => {
                assert_eq!(image.id, 4864340);
                assert_eq!(image.title, "API Test Client Test Image");
                assert_eq!(image.url, "https://www.worldanvil.com/uploads/images/61aa0764670613ff65d712413279eed7.webp");
                assert_eq!(image.state, State::public);
            }
            Err(err) => {
                println!("{}", err);
                assert!(false, "Expected success result");
            }
        }
    }

    #[test]
    async fn test_image_get_full_success() {
        let api_client = create_user_endpoint_and_id().await;
        let result = api_client.get::<Image>("4864340".to_string(), Granularity::Two).await;
        match result {
            Ok(image) => {
                assert_eq!(image.id, 4864340);
                assert_eq!(image.title, "API Test Client Test Image");
                assert_eq!(image.url, "https://www.worldanvil.com/uploads/images/61aa0764670613ff65d712413279eed7.webp");
                assert_eq!(image.state, State::public);
                assert_eq!(image.likes, Some(1));
            }
            Err(err) => {
                println!("{}", err);
                assert!(false, "Expected success result");
            }
        }
    }

    #[test]
    async fn test_image_patch_success() {
        let api_client = create_user_endpoint_and_id().await;
        let result = api_client.patch::<ImagePatch>("4876918".to_string(),
                                                    ImagePatch::new()
                                                        .title("API Test Image for Update 1")
                                                        .state(State::private)
                                                        .subscriber_groups(vec![])
                                                        .tags(Some("#tag2,#tag1,#tag3"))
                                                        .description(Some("This is an updated description."))
                                                        .alt(Some("This is an updated alt."))
                                                        .credit_artist_name(Some("Test Artist"))
                                                        .credit_artist_website(Some("https://www.worldanvil.com"))
                                                        .credit_art_title(Some("Test Art Title"))
                                                        .credit_art_url(Some("https://www.worldanvil.com"))
                                                        .is_featured(true)
                                                        .link_url(Some("https://www.worldanvil.com"))
                                                        .article(Some("5706c7f2-72ad-4e1c-a0e8-dc7480fcda31"))
                                                        .world("daae0a12-f3c3-4978-b571-b5313e3c1741")
                                                        .character(Some("c8908d86-82bb-4d88-a4da-5bd81f366a60"))).await;

        if result.is_err() {
            println!("{}", result.err().unwrap());
            assert!(false, "Expected success result");
        }

        let image_result = api_client.get::<Image>("4876918".to_string(), Granularity::Two).await;
        match image_result {
            Ok(image) => {
                assert_eq!(image.id, 4876918);
                assert_eq!(image.title, "API Test Image for Update 1");
                assert_eq!(image.url, "https://www.worldanvil.com/uploads/maps/3baa38076250d6d9fbc8c2d367ebad39.png");
                assert_eq!(image.state, State::private);
                assert_eq!(image.tags, Some("#tag2,#tag1,#tag3".to_string()));
                assert_eq!(image.width, Some(255));
                assert_eq!(image.height, Some(255));
                assert_eq!(image.extension, Some("ext".to_string()));
                assert_eq!(image.description, Some("This is an updated description.".to_string()));
                assert_eq!(image.alt, Some("This is an updated alt.".to_string()));
                assert_eq!(image.credit_artist_name, Some("Test Artist".to_string()));
                assert_eq!(image.credit_artist_website, Some("https://www.worldanvil.com".to_string()));
                assert_eq!(image.credit_art_title, Some("Test Art Title".to_string()));
                assert_eq!(image.credit_art_url, Some("https://www.worldanvil.com".to_string()));
                assert_eq!(image.is_featured, Some(true));
                assert_eq!(image.link_url, Some("https://www.worldanvil.com".to_string()));
                assert_eq!(image.article, Some(
                    Reference {
                        id: "5706c7f2-72ad-4e1c-a0e8-dc7480fcda31".to_string().into(),
                        title: "Session Report".to_string(),
                        slug: Some("session-report-report".to_string()),
                        state: Some(State::public),
                        is_wip: Some(true),
                        is_draft: Some(false),
                        entity_class: EntityClass::Report,
                        icon: Some("fa-solid fa-clipboard".to_string()),
                        url: Some("http://www.worldanvil.com/w/api-client-test-world-soullink/a/session-report-report".to_string()),
                        subscriber_groups: vec![],
                        tags: None,
                        update_date: image.article.clone().unwrap().update_date,
                    }
                ));
                assert_eq!(image.owner, Some(Reference {
                    id: "42eb1d6a-021b-49b4-bbbb-f7ddf6b135a4".to_string().into(),
                    title: "SoulLink".to_string(),
                    slug: None,
                    state: None,
                    is_wip: None,
                    is_draft: None,
                    entity_class: EntityClass::User,
                    icon: "fa-solid fa-user".to_string().into(),
                    url: "http://www.worldanvil.com/author/SoulLink".to_string().into(),
                    subscriber_groups: vec![],
                    tags: None,
                    update_date: None,
                }));
                assert_eq!(image.world, Some(
                    Reference {
                        id: "daae0a12-f3c3-4978-b571-b5313e3c1741".to_string().into(),
                        title: "API Client Test World".to_string(),
                        slug: "api-client-test-world-soullink".to_string().into(),
                        state: State::public.into(),
                        is_wip: None,
                        is_draft: None,
                        entity_class: EntityClass::World,
                        icon: "fa-solid fa-globe-stand".to_string().into(),
                        url: "http://www.worldanvil.com/w/api-client-test-world-soullink".to_string().into(),
                        subscriber_groups: vec![
                            Reference { id: StringId("35113a58-c76d-4bcc-a5cb-ae26a2697871".to_string()), title: "A Secret Subscriber Group".to_string(), slug: None, state: None, is_wip: None, is_draft: None, entity_class: SubscriberGroup, icon: None, url: None, subscriber_groups: vec![], tags: None, update_date: None },
                            Reference { id: StringId("85861836-3663-4092-a05c-16549cd54234".to_string()), title: "A Secret Subscriber Group".to_string(), slug: None, state: None, is_wip: None, is_draft: None, entity_class: SubscriberGroup, icon: None, url: None, subscriber_groups: vec![], tags: None, update_date: None },
                            Reference { id: StringId("dda7d97a-873d-47a2-9803-c63a365637cf".to_string()), title: "A Secret Subscriber Group".to_string(), slug: None, state: None, is_wip: None, is_draft: None, entity_class: SubscriberGroup, icon: None, url: None, subscriber_groups: vec![], tags: None, update_date: None },
                            Reference { id: StringId("01229cba-326d-4f07-9a59-6ddb1708b934".to_string()), title: "Adventurers of API Test Campaign Party".to_string(), slug: None, state: None, is_wip: None, is_draft: None, entity_class: SubscriberGroup, icon: None, url: None, subscriber_groups: vec![], tags: None, update_date: None },
                            Reference { id: StringId("4baae79e-843d-4909-833f-2280963ad403".to_string()), title: "API Test Campaign Players".to_string(), slug: None, state: None, is_wip: None, is_draft: None, entity_class: SubscriberGroup, icon: None, url: None, subscriber_groups: vec![], tags: None, update_date: None },
                            Reference { id: StringId("193a7518-7206-4a85-94de-26a380cf0d69".to_string()), title: "SoulLink".to_string(), slug: None, state: None, is_wip: None, is_draft: None, entity_class: SubscriberGroup, icon: None, url: None, subscriber_groups: vec![], tags: None, update_date: None },
                            Reference { id: StringId("11e5081e-b209-490e-8733-b6f85b0c60d4".to_string()), title: "The A Team Party".to_string(), slug: None, state: None, is_wip: None, is_draft: None, entity_class: SubscriberGroup, icon: None, url: None, subscriber_groups: vec![], tags: None, update_date: None }],
                        tags: Some("#TEST,#DONOTCLICK".to_string()),
                        update_date: image.world.clone().unwrap().update_date.clone().into(),
                    }),
                );
                assert_eq!(image.character, Some(
                    Reference {
                        id: "c8908d86-82bb-4d88-a4da-5bd81f366a60".to_string().into(),
                        title: "John".to_string(),
                        slug: Some("23e25478-64f6-41d1-b305-1b119f7b898a".to_string()),
                        state: State::visible.into(),
                        is_wip: None,
                        is_draft: None,
                        entity_class: EntityClass::Character,
                        icon: "fa-solid fa-mask".to_string().into(),
                        url: "http://www.worldanvil.com/hero/23e25478-64f6-41d1-b305-1b119f7b898a".to_string().into(),
                        subscriber_groups: vec![],
                        tags: None,
                        update_date: None,
                    }
                ));
            }
            Err(err) => {
                println!("{}", err);
                assert!(false, "Expected success result");
            }
        }
    }
}