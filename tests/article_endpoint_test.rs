#[cfg(test)]
mod tests {
    use std::env;
    use tokio::test;
    use waapiclient::ApiClient;
    use waapiclient::enums::{Granularity, State};
    use waapiclient::schemas::Article;
    use waapiclient::schemas::article::{ArticleCreate, ArticleUpdate};

    async fn create_user_endpoint_and_id() -> ApiClient {
        let application_key = env::var("WORLD_ANVIL_APPLICATION_KEY").unwrap_or_else(|_| "test".to_string());
        let authentication_token = env::var("WORLD_ANVIL_AUTHENTICATION_TOKEN").unwrap_or_else(|_| "test".to_string());

        ApiClient::new(
            "World Anvil Api Client Test/0.1.0 (Rust)",
            &application_key,
            &authentication_token,
        )
    }

    #[test]
    async fn test_article_lifecycle() {
        let api_client = create_user_endpoint_and_id().await;
        let article = match api_client.put::<ArticleCreate>(ArticleCreate::new(
            "Test Article",
            "article",
            "daae0a12-f3c3-4978-b571-b5313e3c1741",
        )).await {
            Ok(article) => {
                assert_eq!(article.title, "Test Article");
                article
            }
            Err(err) => {
                println!("{}", err);
                assert!(false, "Expected success result");
                panic!();
            }
        };


        match api_client.patch::<ArticleUpdate>(article.id.clone().into(), ArticleUpdate::new()
            .content("Test Content")
            .state(State::private)
        ).await {
            Ok(article) => {
                assert_eq!(article.title, "Test Article");
            }
            Err(err) => {
                println!("{}", err);
                assert!(false, "Expected success result");
            }
        }


        match api_client.get::<Article>(article.id.clone().into(), Granularity::Three).await {
            Ok(article) => {
                assert_eq!(article.title, "Test Article");
                assert_eq!(article.content, Some("Test Content".to_string()));
                assert_eq!(article.state, State::private);
            }
            Err(err) => {
                println!("{}", err);
                assert!(false, "Expected success result");
            }
        }

        match api_client.delete::<Article>(article.id.clone().into()).await {
            Ok(_) => {
                assert_eq!(true, true);
            }
            Err(err) => {
                println!("{}", err);
                assert!(false, "Expected success result");
            }
        };
    }
}