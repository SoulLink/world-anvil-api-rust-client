#[cfg(test)]
mod tests {

    use std::env;
    use tokio::test;
    use waapiclient::ApiClient;

    #[test]
    async fn test_read_identity_success() {
        // Create an instance of ApiClient for testing.
        let application_key = env::var("WORLD_ANVIL_APPLICATION_KEY").unwrap_or_else(|_| "test".to_string());
        let authentication_token = env::var("WORLD_ANVIL_AUTHENTICATION_TOKEN").unwrap_or_else(|_| "test".to_string());
        // Create an instance of ApiClient with the environment variables.
        let api_client = ApiClient::new(
            "World Anvil Api Client Test/0.1.0 (Rust)",
            &application_key,
            &authentication_token,
        );
        // Call the read_identity method and match on the result.
        match api_client.identity().await {
            Ok(response) => {
                assert_eq!(response.id, "42eb1d6a-021b-49b4-bbbb-f7ddf6b135a4")
            }
            Err(err) => {
                println!("{}", err);
                assert!(false, "Expected success result");
            }
        }
    }

    #[test]
    async fn test_read_identity_no_auth() {
        // Create an instance of ApiClient for testing.
        let api_client = ApiClient::new(
            "World Anvil Api Client Test/0.1.0 (Rust)",
            "test",
            "test",
        );
        // Call the read_identity method and match on the result.
        match api_client.identity().await {
            Ok(_) => {
                assert!(false, "Expected error result");
            }
            Err(err) => {
                assert_eq!(err.to_string(), "Unauthorized (401). No valid authentication provided.")
            }
        }
    }
}