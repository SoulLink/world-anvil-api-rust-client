#[cfg(test)]
mod tests {

use std::env;
    use tokio::test;
    use waapiclient::ApiClient;
    use waapiclient::enums::Granularity;
    use waapiclient::schemas::world::World;
    use waapiclient::schemas::WorldCreate;

    async fn create_api_client() -> ApiClient {
        let application_key = env::var("WORLD_ANVIL_APPLICATION_KEY").unwrap_or_else(|_| "test".to_string());
        let authentication_token = env::var("WORLD_ANVIL_AUTHENTICATION_TOKEN").unwrap_or_else(|_| "test".to_string());

        ApiClient::new(
            "World Anvil Api Client Test/0.1.0 (Rust)",
            &application_key,
            &authentication_token,
        )
    }

    #[test]
    async fn test_world_get() {
        let api_client = create_api_client().await;
        match api_client.get::<World>("daae0a12-f3c3-4978-b571-b5313e3c1741".to_string(), Granularity::Reference).await {
            Ok(world) => {
                assert_eq!(world.id, "daae0a12-f3c3-4978-b571-b5313e3c1741");
                assert_eq!(world.title, "API Client Test World");
                assert_eq!(world.url, "http://www.worldanvil.com/w/api-client-test-world-soullink");
            }
            Err(err) => {
                println!("{}", err);
                assert!(false, "Expected success result");
            }
        }
    }

    #[test]
    async fn test_world_put() {
        let api_client = create_api_client().await;

        match api_client.put::<WorldCreate>(
            WorldCreate::new("Test World")
        ).await {
            Ok(world) => {
                api_client.delete::<World>(world.id.clone().into()).await.unwrap();
                assert_eq!(world.title, "Test World");
                assert_eq!(world.url, "http://www.worldanvil.com/w/test-world-soullink-1".to_string().into());


            }
            Err(err) => {
                println!("{}", err);
                assert!(false, "Expected success result");
            }
        };


    }
}